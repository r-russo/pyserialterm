import sys

from configparser import ConfigParser

from PyQt5.QtWidgets import (QApplication, QMainWindow, QStyle, QAction,
                             QDialog, QFileDialog, QMessageBox)

from main_widget import MainWidget
from dialogs import SetupPortDialog


class App(QMainWindow):
    INI_HEADER_RX = 'RX'
    INI_HEADER_TX = 'TX'

    def __init__(self):
        super().__init__()
        self.title = "Serial Com"
        self.left = 10
        self.top = 10
        self.width = 640
        self.height = 480
        self.initUI()
        self.update_status_bar()

    def create_bars(self):
        # Icons
        new_icon = self.style().standardIcon(QStyle.SP_FileIcon)
        load_icon = self.style().standardIcon(QStyle.SP_DirOpenIcon)
        save_icon = self.style().standardIcon(QStyle.SP_DialogSaveButton)
        find_icon = self.style().standardIcon(QStyle.SP_FileDialogContentsView)
        clear_icon = self.style().standardIcon(QStyle.SP_DialogCloseButton)
        select_icon = self.style().standardIcon(QStyle.SP_ComputerIcon)
        start_icon = self.style().standardIcon(QStyle.SP_MediaPlay)
        stop_icon = self.style().standardIcon(QStyle.SP_MediaStop)

        # File actions
        new_action = QAction(new_icon, '&New workspace', self)
        new_action.setShortcut('Ctrl-N')
        new_action.triggered.connect(self.setup_new_workspace)
        load_action = QAction(load_icon, '&Load workspace', self)
        save_action = QAction(save_icon, '&Save workspace', self)
        import_action = QAction('&Import sequences', self)
        import_action.triggered.connect(self.import_sequences)
        export_action = QAction('&Export sequences', self)
        export_action.triggered.connect(self.export_sequences)
        print_action = QAction('&Print', self)
        exit_action = QAction('&Exit', self)

        # Edit actions
        seq_action = QAction('&Add sequence', self)
        find_action = QAction(find_icon, '&Find in monitor', self)
        clear_action = QAction(clear_icon, '&Clear monitor', self)
        clear_action.triggered.connect(self.clear_monitor)

        # Communication actions
        select_action = QAction(select_icon, 'S&elect port', self)
        select_action.triggered.connect(self.setup_port_dialog)
        start_action = QAction(start_icon, '&Start', self)
        start_action.triggered.connect(lambda:
                                       self.start_stop_communication(True))
        stop_action = QAction(stop_icon, 'S&top', self)
        stop_action.triggered.connect(lambda:
                                      self.start_stop_communication(False))

        # Menu bar
        menu_bar = self.menuBar()
        file_menu = menu_bar.addMenu("&File")
        file_menu.addAction(new_action)
        file_menu.addAction(load_action)
        file_menu.addAction(save_action)
        file_menu.addAction(import_action)
        file_menu.addAction(export_action)
        file_menu.addSeparator()
        file_menu.addAction(print_action)
        file_menu.addSeparator()
        file_menu.addAction(exit_action)
        edit_menu = menu_bar.addMenu("&Edit")
        edit_menu.addAction(seq_action)
        edit_menu.addAction(find_action)
        edit_menu.addAction(clear_action)
        comm_menu = menu_bar.addMenu("&Communication")
        comm_menu.addAction(select_action)
        comm_menu.addAction(start_action)
        comm_menu.addAction(stop_action)

        # Toolbar
        toolbar = self.addToolBar('Toolbar')
        toolbar.addAction(new_action)
        toolbar.addAction(load_action)
        toolbar.addAction(save_action)
        toolbar.addSeparator()
        toolbar.addAction(find_action)
        toolbar.addAction(clear_action)
        toolbar.addSeparator()
        toolbar.addAction(select_action)
        toolbar.addAction(start_action)
        toolbar.addAction(stop_action)

    def update_status_bar(self):
        if self.main_widget.serial_port.port() is None:
            message = "Please configure a serial port"
        else:
            port = self.main_widget.serial_port.port()
            baudrate = self.main_widget.serial_port.baudrate()
            is_open = self.main_widget.serial_port.isOpen()
            if is_open:
                message = "Connected | "
            else:
                message = "Disconnected | "
            message += "Port %s at %d baud" % (port, baudrate)

        self.statusBar().showMessage(message)

    def initUI(self):
        self.setWindowTitle(self.title)
        self.setGeometry(self.left, self.top,
                         self.width, self.height)

        self.main_widget = MainWidget(self)
        self.setCentralWidget(self.main_widget)
        self.create_bars()
        self.main_widget.serial_port.portChange.connect(self.update_status_bar)

        self.show()

    def clear_monitor(self):
        self.main_widget.right_panel.clear()

    def setup_new_workspace(self):
        self.clear_monitor()
        self.main_widget.left_panel.clear_sequences()

    def import_sequences(self):
        seq_file = QFileDialog.getOpenFileName(self, 'Open sequences file',
                                               '.', "INI Files (*.ini)")

        if seq_file[0]:
            config = ConfigParser()
            config.read(seq_file[0])

            for section in config.sections():
                if section == self.INI_HEADER_TX:
                    tx = True
                elif section == self.INI_HEADER_RX:
                    tx = False
                else:
                    continue
                for name, seq in config[section].items():
                    self.main_widget.left_panel.add_sequence(name, seq,
                                                             tx)

    def export_sequences(self):
        rows_tx = self.main_widget.left_panel.get_number_of_rows(tx=True)
        rows_rx = self.main_widget.left_panel.get_number_of_rows(tx=False)

        if rows_tx == 1 and rows_rx == 1:
            QMessageBox.information(self, "No sequences to export",
                                    "There are no sequences to export. Please"
                                    " add some with the add button.")
            return

        d = QFileDialog(self)
        d.setDefaultSuffix('ini')
        d.setNameFilters(['INI Files (*.ini)'])
        d.setAcceptMode(QFileDialog.AcceptSave)
        d.setWindowTitle('Save sequences')

        if d.exec_() == QDialog.Accepted:
            config = ConfigParser()
            seq_file = d.selectedFiles()
            left_panel = self.main_widget.left_panel

            if rows_tx > 1:
                config[self.INI_HEADER_TX] = {}

                for row in range(1, rows_tx):
                    name = left_panel.get_name_widget(row, tx=True).text()
                    seq = left_panel.get_seq_widget(row, tx=True).text()

                    config[self.INI_HEADER_TX][name] = seq

            if rows_rx > 1:
                config[self.INI_HEADER_RX] = {}

                for row in range(1, rows_rx):
                    name = left_panel.get_name_widget(row, tx=False).text()
                    seq = left_panel.get_seq_widget(row, tx=False).text()
                    config[self.INI_HEADER_RX][name] = seq

            with open(seq_file[0], 'w') as f:
                config.write(f)

    def setup_port_dialog(self, tx=True):
        d = SetupPortDialog(self)
        if (d.exec_()):
            self.main_widget.serial_port.setup(d.dict_args)

    def start_stop_communication(self, start=True):
        if start:
            if self.main_widget.serial_port.open():
                # self.main_widget.serial_port.start()
                pass
            else:
                QMessageBox.critical("Can't open port")
        else:
            self.main_widget.serial_port.close()


if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = App()
    sys.exit(app.exec_())
