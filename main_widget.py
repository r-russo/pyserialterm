from PyQt5.QtWidgets import (QWidget, QVBoxLayout, QHBoxLayout, QPushButton,
                             QGroupBox, QTextBrowser, QLabel, QGridLayout,
                             QScrollArea)

from PyQt5.QtCore import Qt, pyqtSignal

from datetime import datetime
from serial_port import SerialPort
from dialogs import AddSequenceDialog

clr_timestamp = "green"
clr_rx = "red"
clr_tx = "blue"


class MainWidget(QWidget):
    def __init__(self, parent):
        super(QWidget, self).__init__(parent)
        self.left_panel = LeftPanel(self)
        self.right_panel = RightPanel(self)
        self.serial_port = SerialPort(self)
        self.serial_port.start()

        self.left_panel.txMessage.connect(self.right_panel.write)
        self.left_panel.txMessage.connect(self.serial_port.send)
        self.serial_port.recieveChar.connect(self.right_panel.write)

        layout = QHBoxLayout()
        layout.addWidget(self.left_panel)
        layout.addWidget(self.right_panel)

        self.setLayout(layout)


class LeftPanel(QWidget):
    txMessage = pyqtSignal(str)
    COLS_TX = 3
    COLS_RX = 2

    def __init__(self, parent):
        super(QWidget, self).__init__(parent)

        self.setupLayouts()

        lblTx = QLabel("Send sequence")
        btnAdd_tx = QPushButton('Add')
        btnAdd_tx.clicked.connect(lambda: self.add_sequence_dialog(True))

        lblRx = QLabel("Recieve sequence")
        btnAdd_rx = QPushButton('Add')
        btnAdd_rx.clicked.connect(lambda: self.add_sequence_dialog(False))

        widget_tx = QWidget()
        widget_tx.setLayout(self.tx_layout)
        scroll_tx = QScrollArea()
        scroll_tx.setWidget(widget_tx)
        scroll_tx.setWidgetResizable(True)

        widget_rx = QWidget()
        widget_rx.setLayout(self.rx_layout)
        scroll_rx = QScrollArea()
        scroll_rx.setWidget(widget_rx)
        scroll_rx.setWidgetResizable(True)

        layout = QVBoxLayout()
        layout.addWidget(lblTx)
        layout.addWidget(scroll_tx)
        layout.addWidget(btnAdd_tx)
        layout.addWidget(lblRx)
        layout.addWidget(scroll_rx)
        layout.addWidget(btnAdd_rx)
        self.setLayout(layout)

    def setupLayouts(self):
        self.tx_layout = QGridLayout()
        self.tx_layout.setAlignment(Qt.AlignTop)
        lblName = QLabel('Name')
        lblSeq = QLabel('Sequence')
        lblSend = QLabel('')
        self.tx_layout.addWidget(lblName, 0, 0)
        self.tx_layout.addWidget(lblSeq, 0, 1)
        self.tx_layout.addWidget(lblSend, 0, 2)

        self.rx_layout = QGridLayout()
        self.rx_layout.setAlignment(Qt.AlignTop)
        lblName = QLabel('Name')
        lblSeq = QLabel('Sequence')
        self.rx_layout.addWidget(lblName, 0, 0)
        self.rx_layout.addWidget(lblSeq, 0, 1)

    def add_sequence_dialog(self, tx=True):
        d = AddSequenceDialog(self)
        if (d.exec_()):
            self.add_sequence(d.txtName.text(), d.txtSeq.toPlainText(), tx)

    def add_sequence(self, name, seq, tx=True):
        lblName = QLabel(name)
        lblSeq = QLabel(seq)
        rows = self.get_number_of_rows(tx)
        if tx:
            btnSend = QPushButton("->")
            btnSend.clicked.connect(lambda: self.send_sequence(rows))
            self.tx_layout.addWidget(lblName, rows, 0)
            self.tx_layout.addWidget(lblSeq, rows, 1)
            self.tx_layout.addWidget(btnSend, rows, 2)
        else:
            self.rx_layout.addWidget(lblName, rows, 0)
            self.rx_layout.addWidget(lblSeq, rows, 1)

    def get_name_widget(self, row, tx=True):
        if tx:
            return self.tx_layout.itemAt(row * self.COLS_TX).widget()
        else:
            return self.rx_layout.itemAt(row * self.COLS_RX).widget()

    def get_seq_widget(self, row, tx=True):
        if tx:
            return self.tx_layout.itemAt(row * self.COLS_TX + 1).widget()
        else:
            return self.rx_layout.itemAt(row * self.COLS_RX + 1).widget()

    def get_button_widget(self, row, tx=True):
        if tx:
            return self.tx_layout.itemAt(row * self.COLS_TX + 2).widget()
        else:
            return self.rx_layout.itemAt(row * self.COLS_RX + 2).widget()

    def get_number_of_rows(self, tx=True):
        if tx:
            return self.tx_layout.count() // self.COLS_TX
        else:
            return self.rx_layout.count() // self.COLS_RX

    def send_sequence(self, row):
        self.txMessage.emit(self.get_seq_widget(row).text())

    def clear_sequences(self):
        rows_tx = self.get_number_of_rows(tx=True)
        rows_rx = self.get_number_of_rows(tx=False)

        for i in reversed(range(self.COLS_TX, rows_tx * self.COLS_TX)):
            w = self.tx_layout.itemAt(i).widget()
            self.tx_layout.removeWidget(w)
            w.deleteLater()

        for i in reversed(range(self.COLS_RX, rows_rx * self.COLS_RX)):
            w = self.rx_layout.itemAt(i).widget()
            self.tx_layout.removeWidget(w)
            w.deleteLater()


class RightPanel(QWidget):
    def __init__(self, parent):
        super(QWidget, self).__init__(parent)

        grpBox_mon = QGroupBox()
        grpBox_mon.setTitle("Monitor")

        self.monitor = QTextBrowser()
        lay_grp = QVBoxLayout()
        lay_grp.addWidget(self.monitor)

        grpBox_mon.setLayout(lay_grp)

        layout = QVBoxLayout()
        layout.addWidget(grpBox_mon)
        self.setLayout(layout)

        self.last_timestamp = datetime.now()

    def write(self, message, tx=True):
        timestamp = datetime.now()
        if tx:
            state_tag = 'TX'
            clr_message = clr_tx
        else:
            state_tag = 'RX'
            clr_message = clr_rx

        # Replace special characters
        message = message.replace('\n', '&#60;NL&#62;')
        message = message.replace('\r', '&#60;CR&#62;')

        html = ('<br /><font color="%s">%s [%s]</font>'
                ' <font color="%s">%s</font>') % (clr_timestamp,
                                                  str(timestamp),
                                                  state_tag,
                                                  clr_message,
                                                  message)

        self.monitor.insertHtml(html)
        self.monitor.ensureCursorVisible()

    def clear(self):
        self.monitor.setText('')
