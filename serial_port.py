from serial import Serial, SerialException

from PyQt5.QtCore import pyqtSignal, QThread


class SerialPort(QThread):
    portChange = pyqtSignal()
    recieveChar = pyqtSignal(str, bool)
    BLOCK_SIZE = 128
    TIMEOUT = 1

    def __init__(self, parent):
        super(QThread, self).__init__(parent)
        self.serial = Serial(timeout=self.TIMEOUT)
        self.is_open = False

    def setup(self, dict_args):
        if 'baudrate' in dict_args.keys():
            self.serial.baudrate = dict_args['baudrate']
        if 'port' in dict_args.keys():
            self.serial.port = dict_args['port']
        if 'bytesize' in dict_args.keys():
            self.serial.bytesize = dict_args['bytesize']
        if 'parity' in dict_args.keys():
            self.serial.parity = dict_args['parity']
        if 'stopbits' in dict_args.keys():
            self.serial.stopbits = dict_args['stopbits']
        if 'xonxoff' in dict_args.keys():
            self.serial.xonoff = dict_args['xonxoff']
        if 'rtscts' in dict_args.keys():
            self.serial.rtscts = dict_args['rtscts']
        if 'dsrdtr' in dict_args.keys():
            self.serial.dsrdtr = dict_args['dsrdtr']
        self.portChange.emit()

    def run(self):
        while True:
            self.sleep(.5)
            if self.isOpen():
                c = self.serial.read(size=self.BLOCK_SIZE).decode('utf-8')
                if c != '':
                    self.recieveChar.emit(c, False)

    def send(self, message):
        if self.isOpen():
            self.serial.write(message.encode('utf-8'))

    def open(self):
        try:
            self.serial.open()
            self.is_open = True
            self.portChange.emit()
        except SerialException:
            self.portChange.emit()
            self.is_open = False
            return False

        return True

    def close(self):
        if self.serial.isOpen():
            self.serial.close()
            self.is_open = False
            self.portChange.emit()

    def isOpen(self):
        return self.is_open

    def port(self):
        return self.serial.port

    def baudrate(self):
        return self.serial.baudrate


if __name__ == '__main__':
    serial_port = SerialPort('/dev/ttyUSB0', 9600)
    serial_port.open()
    serial_port.run()
