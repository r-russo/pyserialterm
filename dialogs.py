from PyQt5.QtWidgets import (QDialog, QLabel, QLineEdit, QTextEdit, QCheckBox,
                             QSpinBox, QDialogButtonBox, QFormLayout,
                             QGroupBox, QComboBox, QVBoxLayout)

from serial.tools.list_ports import comports

import serial


#configurar con accept reject
class AddSequenceDialog(QDialog):
    def __init__(self, parent):
        super(QDialog, self).__init__(parent)

        lblName = QLabel('Name:')
        self.txtName = QLineEdit()
        lblSeq = QLabel('Sequence:')
        self.txtSeq = QTextEdit()
        self.chkRepeat = QCheckBox('Repeat')
        self.spnInterval = QSpinBox()
        btnBox = QDialogButtonBox(QDialogButtonBox.Ok |
                                  QDialogButtonBox.Cancel)
        btnBox.rejected.connect(self.reject)
        btnBox.accepted.connect(self.accept)

        layout = QFormLayout()
        layout.addRow(lblName, self.txtName)
        layout.addRow(lblSeq, self.txtSeq)
        layout.addRow(self.chkRepeat, self.spnInterval)
        layout.addRow(btnBox)

        self.setLayout(layout)


class SetupPortDialog(QDialog):
    def __init__(self, parent):
        super(QDialog, self).__init__(parent)

        lblPort = QLabel('Port:')
        self.cmbPort = QComboBox()
        for p in comports():
            self.cmbPort.addItem(p.device)

        lblBaud = QLabel('Baudrate:')
        self.cmbBaud = QComboBox()
        self.cmbBaud.setEditable(True)
        lblParity = QLabel('Parity:')
        self.cmbParity = QComboBox()
        lblData = QLabel('Data bits:')
        self.cmbData = QComboBox()
        lblStop = QLabel('Stop bits:')
        self.cmbStop = QComboBox()

        for i, baudrate in enumerate([50, 75, 110, 134.5, 150, 300, 600, 1200,
                                      1800, 2400, 4800, 7200, 9600, 14400,
                                      19200, 38400, 56000, 57600, 76800,
                                      115200, 128000, 230400, 256000, 460800]):
            self.cmbBaud.addItem(str(baudrate))
            if baudrate == 9600:
                self.cmbBaud.setCurrentIndex(i)

        for i, parity in enumerate(['None', 'Even', 'Odd', 'Mark', 'Space']):
            self.cmbParity.addItem(parity)
            if parity == 'None':
                self.cmbParity.setCurrentIndex(i)

        for i, databits in enumerate(range(5, 8 + 1)):
            self.cmbData.addItem(str(databits))
            if databits == 8:
                self.cmbData.setCurrentIndex(i)

        for i, stopbits in enumerate([1, 1.5, 2]):
            self.cmbStop.addItem(str(stopbits))
            if stopbits == 1:
                self.cmbStop.setCurrentIndex(i)

        layout_com = QFormLayout()
        layout_com.addRow(lblPort, self.cmbPort)
        layout_com.addRow(lblBaud, self.cmbBaud)
        layout_com.addRow(lblParity, self.cmbParity)
        layout_com.addRow(lblData, self.cmbData)
        layout_com.addRow(lblStop, self.cmbStop)

        self.chkXonXoff = QCheckBox()
        self.chkXonXoff.setText('Xon/Xoff')
        self.chkRTSCTS = QCheckBox()
        self.chkRTSCTS.setText('RTS/CTS')
        self.chkDSRDTR = QCheckBox()
        self.chkDSRDTR.setText('DSR/DTR')

        layout_flow = QVBoxLayout()
        layout_flow.addWidget(self.chkXonXoff)
        layout_flow.addWidget(self.chkRTSCTS)
        layout_flow.addWidget(self.chkDSRDTR)

        frame_com = QGroupBox()
        frame_com.setTitle('Communication')
        frame_com.setLayout(layout_com)

        frame_flow = QGroupBox()
        frame_flow.setTitle('Flow Control')
        frame_flow.setCheckable(True)
        frame_flow.setLayout(layout_flow)

        btnBox = QDialogButtonBox(QDialogButtonBox.Ok |
                                  QDialogButtonBox.Cancel)
        btnBox.rejected.connect(self.reject)
        btnBox.accepted.connect(self.create_dict_and_accept)

        layout = QVBoxLayout()
        layout.addWidget(frame_com)
        layout.addWidget(frame_flow)
        layout.addWidget(btnBox)

        self.setLayout(layout)

    def create_dict_and_accept(self):
        self.dict_args = {}
        self.dict_args['port'] = self.cmbPort.currentText()
        # check if int
        self.dict_args['baudrate'] = int(self.cmbBaud.currentText())
        self.dict_args['bytesize'] = int(self.cmbData.currentText())

        parity = self.cmbParity.currentText()
        if parity == 'None':
            self.dict_args['parity'] = serial.PARITY_NONE
        elif parity == 'Even':
            self.dict_args['parity'] = serial.PARITY_EVEN
        elif parity == 'Odd':
            self.dict_args['parity'] = serial.PARITY_ODD
        elif parity == 'Mark':
            self.dict_args['parity'] = serial.PARITY_MARK
        elif parity == 'Space':
            self.dict_args['parity'] = serial.PARITY_SPACE

        stopbits = self.cmbStop.currentText()
        if stopbits == '1':
            self.dict_args['stopbits'] = serial.STOPBITS_ONE
        elif stopbits == '1.5':
            self.dict_args['stopbits'] = serial.STOPBITS_ONE_POINT_FIVE
        elif stopbits == '2':
            self.dict_args['stopbits'] = serial.STOPBITS_TWO

        self.dict_args['xonxoff'] = self.chkXonXoff.isChecked()
        self.dict_args['rtscts'] = self.chkRTSCTS.isChecked()
        self.dict_args['dsrdtr'] = self.chkDSRDTR.isChecked()

        self.accept()
